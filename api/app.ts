import {use, schema} from 'nexus'
import {prisma} from 'nexus-plugin-prisma'
import {DB} from './db/db'

import {PrismaClient} from "nexus-plugin-prisma/client"

use(prisma({client: {instance: new PrismaClient()}}))

schema.addToContext(() => {
    return {
        DB,
    }
})
