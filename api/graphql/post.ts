import {schema} from "nexus"

schema.objectType({
    name: 'Post',
    definition(t) {
        t.int('id')
        t.string('title')
        t.string('body')
        t.boolean('published')
    }
})

schema.extendType({
    type: "Query",
    definition(t) {
        t.field("posts", {
            type: "Post",
            args: {
                published: schema.booleanArg({required: false}),
                title: schema.stringArg({required: false, default: ''}),
                body: schema.stringArg({required: false, default: ''})
            },
            list: true,
            resolve(_root, args, ctx) {
                const {published, title, body} = args
                if (published) {
                    return ctx.db.post.findMany({
                        where: {
                            AND: {
                                published: {equals: published},
                                title: {contains: title},
                                body: {contains: body}
                            }
                        }
                    })
                } else {
                    return ctx.db.post.findMany({
                        where: {
                            AND: {
                                title: {contains: title},
                                body: {contains: body}
                            }
                        }
                    })
                }
            }
        })
    }
})

schema.extendType({
    type: "Mutation",
    definition(t) {
        t.field("createDraft", {
            type: "Post",
            args: {
                title: schema.stringArg({required: true}),
                body: schema.stringArg({required: true})
            },
            resolve: async (_root, args, ctx) => {
                const {title, body} = args;
                let post = await ctx.db.post.findOne({where: {title}});
                if (post) {
                    throw new Error('post is existed with title:' + title)
                }
                return ctx.db.post.create({
                    data: {
                        title,
                        body,
                        published: false
                    }
                })
            }
        })
        t.field("updateDraft", {
            type: "Post",
            args: {
                title: schema.stringArg({required: false}),
                body: schema.stringArg({required: false}),
                published: schema.booleanArg({required: false}),
                id: schema.intArg({required: true})
            },
            resolve: async (_root, args, ctx) => {
                const {id, title, body, published} = args
                let post = await ctx.db.post.findOne({where: {id}});
                if (!post) {
                    throw new Error('can not find post with id:' + args.id)
                }

                const data: any = {}
                if (published) {
                    data.published = published
                }
                if (body) {
                    data.body = body
                }
                if (title) {
                    data.title = title
                    let post = await ctx.db.post.findOne({where: {title}});
                    if (post && post.id !== id) {
                        throw new Error('post is existed with title:' + title)
                    }
                }

                return ctx.db.post.update({where: {id}, data})
            }
        })
        t.field("deleteDraft", {
            type: "EffectedId",
            args: {
                id: schema.intArg({required: true})
            },
            resolve: async (_root, args, ctx) => {
                const {id} = args
                let post = await ctx.db.post.findOne({where: {id}});
                if (!post) {
                    throw new Error('can not find post with id:' + args.id)
                }

                return ctx.db.post.delete({where: {id}})
            }
        })
    }
})