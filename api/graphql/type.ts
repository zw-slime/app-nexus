import {schema} from "nexus"

schema.objectType({
    name: 'EffectedId',
    definition(t) {
        t.int('id')
    }
})